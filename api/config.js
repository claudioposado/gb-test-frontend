module.exports = {
  PORT: process.env.PORT || '4000',
  ENV: process.env.NODE_ENV || 'development',
  DB: process.env.DB || 'mongodb://mongo:27017/gb-db'
};