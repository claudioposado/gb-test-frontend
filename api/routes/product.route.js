import express from 'express';

import productCtrl from '../controllers/product.controller';

const router = express.Router();

router.route('/')
    .get(productCtrl.list);

router.route('/:productId')
    .get(productCtrl.get);

router.route('/')
    .post(productCtrl.create);

export default router;