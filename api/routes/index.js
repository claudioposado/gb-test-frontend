import express from 'express';
import productRoutes from './product.route';

const router = express.Router();

router.get('/', (req, res) =>
    res.json({status: "ok"})
);

router.use('/products', productRoutes);

export default router;