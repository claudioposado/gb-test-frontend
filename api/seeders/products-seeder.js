var mongoose = require('mongoose');
var Product = require('../models/product.model');

var MONGO_URI = process.env.DB || 'mongodb://mongo:27017/gb-db';
mongoose.connect(MONGO_URI);

// Product.collection.drop();

var products = [
  new Product({
    name: 'televisor 32 LED',
    imageUrl: '/images/sony.jpg',
    price: 19999.99,
    list_price: 29999.99,
    brand: 'SONY',
    category_id: 12345
  }),
  new Product({
    name: 'televisor 50 4K',
    imageUrl: '/images/lg.jpg',
    price: 29999.99,
    list_price: 39999.99,
    brand: 'LG',
    category_id: 12345
  }),
  new Product({
    name: 'Microsoft OFFICE 365 PERSONAL',
    imageUrl: '/images/microsoft.jpg',
    price: 999.99,
    list_price: 999.99,
    brand: 'Microsoft',
    category_id: 123123,
    virtual: true
  })
];

var count = 0;
products.forEach((elem) => {
  elem.save(function(err) {
    if (err) {
      console.log(err);
    } else {
      count++;
      if (count == products.length) quit();
    }
  });
});

function quit() {
  console.log('Added ',products.length,' Product seeds');
  mongoose.disconnect();
}