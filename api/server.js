import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';

import * as config from './config';
import routes from './routes';

var logger = require('morgan');

// bluebird default Promise
Promise = require('bluebird');
mongoose.Promise = Promise;

const mongoUri = config.DB;
mongoose.connect(mongoUri);
mongoose.connection
  .on('connected', () => console.log('mongo connected'))
  .on('error', () => {
      throw new Error(`unable to connect to database: ${mongoUri}`);
  });

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(logger('dev'));

app.use('/api', routes);

app.listen(config.PORT);