var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    'name': {type:String, required:true},
    'imageUrl': {type:String, required:false},
    'price': {type: Number, required: true},
    'list_price': {type: Number, required: true},
    'brand': {type:String, required:true},
    'category_id': {type: Number, required: true},
    'virtual': {type: Boolean, required: false}
});


module.exports = mongoose.model('Product', productSchema);