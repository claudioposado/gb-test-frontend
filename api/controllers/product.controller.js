import Product from '../models/product.model';

/**
 * Get Product by id
 * @param {*} req 
 * @param {*} res 
 * @returns {Product}
 */
function get(req, res) {
  Product.findOne({
      id: req.params.productId
    })
    .then(product => res.json(product))
    .catch(e => next(e));
}

function list(req, res) {
  let query = {};
  if (req.query.name) query.name = {
    $regex: req.query.name,
    '$options': 'i'
  };
  Product.find(query)
    .then(products => res.json(products))
    .catch(e => next(e));

    console.log('Product List');
}

function create(req, res) {
  req.accepts('application/json');

  var product = {
    name: req.body.name,
    imageUrl: req.body.imageUrl,
    price: req.body.price,
    list_price: req.body.list_price,
    brand: req.body.brand,
    category_id: req.body.category_id,
    virtual: req.body.virtual
  };

  var data = new Product(product);
  data.save(function (err) {
    if (err) {
      res.status(500).send();
    } else {
      res.status(201).json(data);
      //
      console.log('Product Added:');
      console.log(product);
      console.log('==============');
    }
  });
}

export default {
  get,
  list,
  create
};