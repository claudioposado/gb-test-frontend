FROM node:alpine
WORKDIR /app
ADD /api/package.json /app
RUN npm install
ADD /api /app
#CMD node server.js
EXPOSE 4000