import React, { Component } from 'react';
import ProductList from '../components/ProductList/ProductList';
import ProductForm from '../components/ProductForm/ProductForm';
import './ProductPage.css';

class ProductPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
        showList: true
    };
  }

  addNewProduct = () => {
    this.setState({showList:false});
  }

  backToList = () => {
    this.setState({showList:true});
  }

  render() {
    return (
      <section className="products">
      {
        this.state.showList ? (
          <div>
            <button className="btn" onClick={this.addNewProduct}>Nuevo Producto</button>
            <ProductList />
          </div>
        ) : (
          <ProductForm  backToList={this.backToList}/>
        )
      }
      </section>
    );
  }
}

export default ProductPage;
