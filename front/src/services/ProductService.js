import axios from 'axios';

// const BASE_URL = '//localhost:4000/api';
// const BASE_URL = 'http://192.168.99.100:4000/api'; // use docker machine ip with docker toolbox
const BASE_URL = process.env.REACT_API_URL || 'http://localhost:4000/api';

export default {

  addProduct(productData) {
    return axios.post(`${BASE_URL}/products`, productData);
  },

  getProductDetailById(id) {
    return axios.get(`${BASE_URL}/products/${id}`);
  },

  getProductList() {
    return axios.get(`${BASE_URL}/products`);
  }

};
