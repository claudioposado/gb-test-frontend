import React, { Component } from 'react';

import Header from './components/Header/Header';
import ProductsPage from './views/ProductPage';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header /> 
        <ProductsPage />
      </div>
    );
  }
}

export default App;
