import React, { Component } from 'react';
import { BarLoader } from 'react-spinners';
import ProductService from '../../services/ProductService';
import ProductListItem from '../ProductListItem/ProductListItem';

import './ProductList.css';

class ProductList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      products: []
    }
  }

  render() {
    const products = this.state.products;
    if(products.length) {
      return (
        <ul className="product-list">
          { products.map((product) => 
            (<ProductListItem key={product._id} product={product} />)
          ) }
        </ul>
      )
    }
    return(
      <div className="loading">
        <BarLoader
        color={'#D1030F'} 
        height={6}
        width={100}
        />
      </div>
    )
  }

  componentDidMount() {
    ProductService.getProductList()
      .then(response => {
        this.setState({
          products: response.data
        })
      })
  }

}

export default ProductList;