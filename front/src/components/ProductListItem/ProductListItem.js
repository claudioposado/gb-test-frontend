import React from 'react';
import './ProductListItem.css';

const calcPercent = (props) => (
  Math.ceil(100 - (props.product.price / props.product.list_price * 100))
) 

const ProductListItem = (props) => (
  <li className="product-item">
    <div className="img-product" style={{backgroundImage: `url(${props.product.imageUrl})`}}></div>
    <div className="container-texts">
      <h3><strong>{props.product.brand}</strong> {props.product.name}</h3>
      <p className="price">${props.product.price}</p>
      {
        props.product.price !== props.product.list_price && (
          <div className="list-price">
            <del>${props.product.list_price}</del>
            <span> %{calcPercent(props)} OFF</span>
          </div>
        )
      }
    </div>
  </li>
);

export default ProductListItem;
