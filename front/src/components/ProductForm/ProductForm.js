import React, { Component } from 'react';
import ProductService from '../../services/ProductService';

import './ProductForm.css';

class ProductForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      sending: false,
      showMsgOk: false,
      showMsgError: false,

      name: '',
      brand: '',
      imageUrl: '',
      price: '',
      list_price: '',
      category_id: '',
      virtual: false,

      touched: {
        name: false,
        brand: false,
        imageUrl: false,
        price: false,
        list_price: false,
        category_id: false
      }
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleBlur = (field) => (evt) => {
    this.setState({
      touched: { ...this.state.touched, [field]: true },
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    this.setState({
      sending: true,
      showMsgOk: false,
      showMsgError: false
    });

    const product = {
      name: this.state.name,
      brand: this.state.brand,
      imageUrl: this.state.imageUrl,
      price: this.state.price,
      list_price: this.state.list_price,
      category_id: this.state.category_id,
      virtual: this.state.virtual
    };

    ProductService.addProduct(product)
      .then(result => {
        console.log(result);
        this.setState({showMsgOk: true});

        setTimeout(() => {
          this.props.backToList();
        }, 4000);
      })
      .catch(error => {
        console.log('error on add product');
        console.log(error);
        this.setState({sending: false, showMsgError: true});
      })

  }


  render () {
    const shouldMarkError = (field) => {
      return this.state.touched[field];
    };

    const { name, brand, imageUrl, price, list_price, category_id, sending } = this.state;
    const isEnabled =
          name.length > 0 &&
          brand.length > 0 &&
          imageUrl.length > 0 &&
          price.length > 0 &&
          list_price.length > 0 &&
          category_id.length > 0 &&
          !sending;

    return (
      <div className="new-product">

        <h3>Agregar Nuevo Producto</h3>

        <form onSubmit={this.handleSubmit}>

          <div className="form-group">
            <label htmlFor="name">Nombre</label>
            <input value={name}
              className={shouldMarkError('name') && name.length === 0 ? "error" : ""}
              onChange={this.handleInputChange} 
              onBlur={this.handleBlur('name')} 
              type="text" name="name" id="name"/>
          </div>

          <div className="form-group">
            <label htmlFor="brand">Marca</label>
            <input value={brand}
              className={shouldMarkError('brand') && brand.length === 0 ? "error" : ""}
              onChange={this.handleInputChange} 
              onBlur={this.handleBlur('brand')} 
              type="text" name="brand" id="brand"/>
          </div>

          <div className="form-group">
            <label htmlFor="imageUrl">Image Url</label>
            <input value={imageUrl}
              className={shouldMarkError('imageUrl') && imageUrl.length === 0 ? "error" : ""}
              onChange={this.handleInputChange} 
              onBlur={this.handleBlur('imageUrl')} 
              type="text" name="imageUrl" id="imageUrl"/>
          </div>

          <div className="form-group">
            <label htmlFor="price">Precio</label>
            <input value={price}
              className={shouldMarkError('price') && price.length === 0 ? "error" : ""}
              onChange={this.handleInputChange} 
              onBlur={this.handleBlur('price')} 
              type="number" name="price" id="price"/>
          </div>

          <div className="form-group">
            <label htmlFor="list_price">Precio de lista</label>
            <input value={list_price}
              className={shouldMarkError('list_price') && list_price.length === 0 ? "error" : ""}
              onChange={this.handleInputChange} 
              onBlur={this.handleBlur('list_price')} 
              type="number" name="list_price" id="list_price"/>
          </div>

          <div className="form-group">
            <label htmlFor="category_id">Category ID</label>
            <input value={category_id}
              className={shouldMarkError('category_id') && category_id.length === 0 ? "error" : ""}
              onChange={this.handleInputChange} 
              onBlur={this.handleBlur('category_id')} 
              type="number" name="category_id" id="category_id"/>
          </div>

          <div className="form-group">
            <button className="btn btn-grey" onClick={this.props.backToList}>Volver al Listado</button>
            <button disabled={!isEnabled} className="btn" type="submit">Enviar</button>

            { this.state.showMsgOk && <p className="msg-ok">Producto creado correctamente.</p>}
            { this.state.showMsgError && <p className="msg-error">Ocurrio un error al crear el producto.</p>}
          </div>


        </form>
      </div>
    )
  }

}

export default ProductForm;
