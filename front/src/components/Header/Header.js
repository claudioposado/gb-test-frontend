import React, { Component } from 'react';
import './Header.css';

export default class Header extends Component {
  render() {
    return (
      <header className="gb-header">
        <img src="//dj4i04i24axgu.cloudfront.net/normandia/statics/logos/logo-garbarino-blanco.svg" className="logo" alt="logo" />
      </header>
    )
  }
}
