## Garbarino Test Frontend

### Instalación / Run
```
$ docker-compose up
```

### Agregar productos a la base (primera vez)
Abrir una segunda terminal y ejecutar el siguiente script: 
```
$ sh seed.sh
```

### Abrir [http://localhost:8080/](http://localhost:8080/) en el browser

---

#### Opcional - si falla docker windows / nginx (en Linux/OS X funciona ok)
lo probe con windows en otra maquina y tuve problemas con el archivo de configuración del nginx. No tuve tiempo de ver bien porque pasaba, por eso deje otra versión del docker-compose que solo ejecuta express y mongodb.
```
$ docker-compose -f docker-compose-dev.yml up
$ cd front
$ yarn 
$ yarn start
```

